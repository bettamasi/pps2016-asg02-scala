package gameComponents;

import model.TaskManager;
import utils.Res;
import utils.Utils;

public class Block extends GameObject {

    private static final int WIDTH = 30;
    private static final int HEIGHT = 30;

    public Block(int xPositionNewBlock, int yPositionNewBlock, TaskManager tmanager) {
        super(xPositionNewBlock, yPositionNewBlock, WIDTH, HEIGHT, tmanager);
        super.setImgObj(Utils.getImage(Res.IMG_BLOCK));
    }

}
