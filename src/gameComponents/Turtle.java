package gameComponents;

import java.awt.Image;

import model.TaskManager;
import utils.Res;
import utils.Utils;

public class Turtle extends GameCharacter implements Runnable {

    private static final int WIDTH = 43;
    private static final int HEIGHT = 50;

    private int dxTurtle;

    public Turtle(int x, int y, TaskManager tmanager) {
        super(x, y, WIDTH, HEIGHT, tmanager);
        super.setCaracterToRight(true);
        super.setCaracterMoving(true);
        this.dxTurtle = 1;
        Image imgTurtle = Utils.getImage(Res.IMG_TURTLE_IDLE);

        Thread chronoTurtle = new Thread(this);
        chronoTurtle.start();
    }

    public void move() {
        this.dxTurtle = getCaracterIsToRight() ? 1 : -1;
        setxPositionObject(getxPositionObject() + this.dxTurtle);
    }

    @Override
    public void run() {
        while (true) {
            if (this.getCaracterIsAlive()) {
                this.move();
                try {
                    int PAUSE = 3;
                    Thread.sleep(PAUSE);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    public void contact(GameObject obj) {
        if (this.hitAhead(obj) && this.getCaracterIsToRight()) {
            this.setCaracterToRight(false);
            this.dxTurtle = -1;
        } else if (this.hitBack(obj) && !this.getCaracterIsToRight()) {
            this.setCaracterToRight(true);
            this.dxTurtle = 1;
        }
    }

    public void contact(GameCharacter pers) {
        if (this.hitAhead(pers) && this.getCaracterIsToRight()) {
            this.setCaracterToRight(false);
            this.dxTurtle = -1;
        } else if (this.hitBack(pers) && !this.getCaracterIsToRight()) {
            this.setCaracterToRight(true);
            this.dxTurtle = 1;
        }
    }

    public Image deadImage() {
        return Utils.getImage(Res.IMG_TURTLE_DEAD);
    }
}
