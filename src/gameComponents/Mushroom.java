package gameComponents;

import java.awt.Image;

import model.TaskManager;
import utils.Res;
import utils.Utils;

public class Mushroom extends GameCharacter implements Runnable {

    private static final int WIDTH = 27;
    private static final int HEIGHT = 30;

    private int offsetX;

    public Mushroom(int x, int y, TaskManager tmanager) {
        super(x, y, WIDTH, HEIGHT, tmanager);
        this.setCaracterToRight(true);
        this.setCaracterMoving(true);
        this.offsetX = 1;
        Image imgMashroom = Utils.getImage(Res.IMG_MUSHROOM_DEFAULT);

        Thread chronoMushroom = new Thread(this);
        chronoMushroom.start();
    }

    public void move() {
        this.offsetX = getCaracterIsToRight() ? 1 : -1;
        setxPositionObject(getxPositionObject() + this.offsetX);

    }

    @Override
    public void run() {
        while (true) {
            if (this.getCaracterIsAlive()) {
                this.move();
                try {
                    int PAUSE = 3;
                    Thread.sleep(PAUSE);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    public void contact(GameObject gameObject) {
        if (this.hitAhead(gameObject) && this.getCaracterIsToRight()) {
            this.setCaracterToRight(false);
            this.offsetX = -1;
        } else if (this.hitBack(gameObject) && !this.getCaracterIsToRight()) {
            this.setCaracterToRight(true);
            this.offsetX = 1;
        }
    }

    public void contact(GameCharacter character) {
        if (this.hitAhead(character) && this.getCaracterIsToRight()) {
            this.setCaracterToRight(false);
            this.offsetX = -1;
        } else if (this.hitBack(character) && !this.getCaracterIsToRight()) {
            this.setCaracterToRight(true);
            this.offsetX = 1;
        }
    }

    public Image deadImage() {
        return Utils.getImage(this.getCaracterIsToRight() ? Res.IMG_MUSHROOM_DEAD_DX : Res.IMG_MUSHROOM_DEAD_SX);
    }
}
