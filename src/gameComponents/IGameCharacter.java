package gameComponents;

import java.awt.*;

/**
 * Created by BettaMasi on 17/03/17.
 */
public interface IGameCharacter extends IGameObject{

    Image walk(String name, int frequency);

    boolean hitAhead(GameObject og);

    boolean hitBack(GameObject og);

    boolean hitBelow(GameObject og);

    boolean isNearby(GameObject obj);

    boolean hitAhead(GameCharacter pers);

    boolean hitBack(GameCharacter pers);

    boolean hitBelow(GameCharacter pers);

    boolean isNearby(GameCharacter pers);


    boolean getCaracterIsAlive();
    void setCaracterAlive(boolean alive);

    boolean getCaracterIsToRight();
    void setCaracterToRight(boolean toRight);

    void setCaracterMoving(boolean moving);

    int getFloorOffsetY();
    void setFloorOffsetY(int floorOffsetY);

    int getHeightLimit();
    void setHeightLimit(int heightLimit);

}
