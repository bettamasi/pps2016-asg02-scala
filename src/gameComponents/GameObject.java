package gameComponents;

import model.TaskManager;

import java.awt.Image;

public class GameObject implements IGameObject{

    private int widthObject, heightObject;
    private int xPositionObject, yPositionObject;

    private Image imageObject;
    public TaskManager tmanager;

    public GameObject(int xPositionNewObj, int yPositionNewObj, int widthNewObj, int heightNewObj, TaskManager tmanager) {
        this.xPositionObject = xPositionNewObj;
        this.yPositionObject = yPositionNewObj;
        this.widthObject = widthNewObj;
        this.heightObject = heightNewObj;

        this.tmanager = tmanager;
    }

    public void move() {
        if (this.tmanager.getxPositionScreen() >= 0) {
            this.xPositionObject = this.xPositionObject - this.tmanager.getMarioMovement();
        }
    }

    public int getWidthObject() {
        return widthObject;
    }
    public int getHeightObject() {
        return heightObject;
    }

    public int getxPositionObject() {
        return xPositionObject;
    }
    public void setxPositionObject(int newxPositionObject) { this.xPositionObject = newxPositionObject;}

    public int getyPositionObject() {
        return yPositionObject;
    }
    public void setyPositionObject(int newyPositionObject) { this.yPositionObject = newyPositionObject; }

    public Image getImgObj() { return this.imageObject; }
    public void setImgObj(Image imgResource) { this.imageObject = imgResource; }

    public TaskManager getTmanager(){ return this.tmanager; }

}
