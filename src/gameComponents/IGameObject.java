package gameComponents;

import java.awt.*;

/**
 * Created by BettaMasi on 17/03/17.
 */
public interface IGameObject {

    void move();

    int getWidthObject();
    int getHeightObject();

    int getxPositionObject();
    int getyPositionObject();

    Image getImgObj();
    void setImgObj(Image imgResource);

}
