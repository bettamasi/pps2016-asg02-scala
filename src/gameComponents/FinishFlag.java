package gameComponents;

import model.TaskManager;
import utils.Res;
import utils.Utils;

/**
 * Created by BettaMasi on 11/04/17.
 */
public class FinishFlag extends GameObject {

    private static final int WIDTH = 49;
    private static final int HEIGHT = 180;

    private static final int  X_POSITION_FLAG = 4650;
    private static final int  Y_POSITION_FLAG = 115;

    public FinishFlag(TaskManager tmanager) {
        super(X_POSITION_FLAG, Y_POSITION_FLAG, WIDTH, HEIGHT, tmanager);
        super.setImgObj(Utils.getImage(Res.IMG_FLAG));
    }


}

