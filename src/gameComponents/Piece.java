package gameComponents;

import model.TaskManager;
import utils.Res;
import utils.Utils;

import java.awt.Image;

public class Piece extends GameObject{

    private static final int WIDTH = 30;
    private static final int HEIGHT = 30;
    private static final int FLIP_FREQUENCY = 100;
    private int counter;

    public Piece(int xPositionNewPiece, int yPositionNewPiece, TaskManager tmanager) {
        super(xPositionNewPiece, yPositionNewPiece, WIDTH, HEIGHT, tmanager);
        super.setImgObj(Utils.getImage(Res.IMG_PIECE1));
    }

    public Image imageOnMovement() {
        return Utils.getImage(++this.counter % FLIP_FREQUENCY == 0 ? Res.IMG_PIECE1 : Res.IMG_PIECE2);
    }
}
