package controller

import game.Audio
import model.TaskManager
import view.CreateView
import java.awt.event._

class Keyboard(var tmanager: TaskManager, var createView: CreateView) extends KeyListener {
  override def keyPressed(e: KeyEvent): Unit = {
    if (tmanager.getMario.getCaracterIsAlive) e.getKeyCode match {
      case KeyEvent.VK_RIGHT =>
        if (this.tmanager.getxPositionScreen == -1) {
          this.tmanager.setxPositionScreen(0)
          this.createView.setXpositionBackgroundUp(-50)
          this.createView.setXpositionBackgroundUnder(750)
        }
        this.tmanager.getMario.setCaracterMoving(true)
        this.tmanager.getMario.setCaracterToRight(true)
        this.tmanager.setMarioMovement(1)

      case KeyEvent.VK_LEFT =>
        if (this.tmanager.getxPositionScreen == 4601) {
          this.tmanager.setxPositionScreen(4600)
          this.createView.setXpositionBackgroundUp(-50)
          this.createView.setXpositionBackgroundUnder(750)
        }
        this.tmanager.getMario.setCaracterMoving(true)
        this.tmanager.getMario.setCaracterToRight(false)
        this.tmanager.setMarioMovement(-1)

      case KeyEvent.VK_UP =>
        this.tmanager.getMario.setJumping(true)
        Audio.playSound("/resources/audio/jump.wav")

      case _ =>
    }
  }

  override def keyReleased(e: KeyEvent): Unit = {
    this.tmanager.getMario.setCaracterMoving(false)
    this.tmanager.setMarioMovement(0)
  }

  override def keyTyped(e: KeyEvent): Unit = {
  }
}