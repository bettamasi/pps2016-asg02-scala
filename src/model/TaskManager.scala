package model

import gameComponents._
import controller.Keyboard
import view.CreateView
import javax.swing._
import java.util
import java.util.Collections


/**
  * Created by BettaMasi on 13/03/17.
  */
object TaskManager{
  val LENGHT_MAP_LEVEL = 4600
  private val POSITION_X_MARIO = 300
  private val POSITION_Y_MARIO = 245
  private val POSITION_X_MUSHROOM = 800
  private val POSITION_Y_MUSHROOM = 263
  private val POSITION_X_TURTLE = 950
  private val POSITION_Y_TURTLE = 243

}

class TaskManager(private var gameFrame: JFrame) extends Thread {

  private val gameView : CreateView = new CreateView(this)
  private val mario : Mario = new Mario(TaskManager.POSITION_X_MARIO, TaskManager.POSITION_Y_MARIO, this)
  private val mushroom : Mushroom = new Mushroom(TaskManager.POSITION_X_MUSHROOM, TaskManager.POSITION_Y_MUSHROOM, this)
  private val turtle : Turtle = new Turtle(TaskManager.POSITION_X_TURTLE, TaskManager.POSITION_Y_TURTLE, this)

  private val finishFlag : FinishFlag = new FinishFlag(this)


  private val newGameObject : CreateGameObjects = new CreateGameObjects(this)
  private val obstacle = new util.ArrayList[GameObject]
  private val pieces = new util.ArrayList[Piece]

  private var xPositionScreen = 0
  private var marioMovement = 0


  gameFrame.add(this.gameView)
  gameFrame.addKeyListener(new Keyboard(this, this.gameView))

  obstacle.addAll(newGameObject.getOstacleTunnel)
  obstacle.addAll(newGameObject.getObstacleBlock)
  pieces.addAll(newGameObject.getObstaclePiece)
  this.xPositionScreen = -1
  this.marioMovement = 0
  this.gameFrame.setVisible(true)


  override def run(): Unit = {
    while ({true}) {
      gameView.repaint()
      this.checkCollisionAgainstObstacle()
      this.checkCollisionBetweenCharacters()
      this.checkCollisionWithPieces()

      this.moveObjects()
      try {
        val SLEEP = 3
        Thread.sleep(SLEEP)
      } catch {
        case e: InterruptedException => println(e)
      }
    }
  }

  private def checkCollisionAgainstObstacle() = {
    for(i <- 0 until obstacle.size){
      if (this.mario.isNearby(this.obstacle.get(i))) this.mario.contact(this.obstacle.get(i))
      if (this.mushroom.isNearby(this.obstacle.get(i))) this.mushroom.contact(this.obstacle.get(i))
      if (this.turtle.isNearby(this.obstacle.get(i))) this.turtle.contact(this.obstacle.get(i))
    }
    if (this.mario.isNearby(this.finishFlag)) this.mario.contact(this.finishFlag)
  }

  private def checkCollisionBetweenCharacters() = {
    if (this.mushroom.isNearby(turtle)) this.mushroom.contact(turtle)
    if (this.turtle.isNearby(mushroom)) this.turtle.contact(mushroom)
    if (this.mario.isNearby(mushroom)) {
      this.mario.contact(mushroom)
    }
    if (this.mario.isNearby(turtle)) this.mario.contact(turtle)
  }

  private def checkCollisionWithPieces() = {
    for(i <- 0 until this.pieces.size()-1){
      if (this.mario.contactPiece(this.pieces.get(i))) {
        this.gameView.playSoundPieces()
        this.pieces.remove(i)
      }
    }
  }

  private def moveObjects() = {
    if (this.xPositionScreen >= 0 && this.xPositionScreen <= TaskManager.LENGHT_MAP_LEVEL) {

      obstacle.forEach((anObstacle : GameObject) => anObstacle.move())

      pieces.forEach((piece : Piece) => piece.move())

      finishFlag.move()
    }
  }

  def getObstacle: util.List[GameObject] = Collections.unmodifiableList(this.obstacle)

  def getPieces: util.List[Piece] = Collections.unmodifiableList(this.pieces)

  def getxPositionScreen: Int = this.xPositionScreen

  def setxPositionScreen(): Unit = this.xPositionScreen += this.marioMovement

  def setxPositionScreen(newxPositionScreen: Int): Unit = this.xPositionScreen = newxPositionScreen

  def getMarioMovement: Int = this.marioMovement

  def setMarioMovement(newMovement: Int): Unit = this.marioMovement = newMovement

  def getMario: Mario = this.mario

  def getMushroom: Mushroom = this.mushroom

  def getTurtle: Turtle = this.turtle

  def getFinishFlag : FinishFlag = this.finishFlag

  def getLenghMap: Int = TaskManager.LENGHT_MAP_LEVEL
}