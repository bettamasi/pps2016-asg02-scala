package model;

import gameComponents.Block;
import gameComponents.Piece;
import gameComponents.Tunnel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by BettaMasi on 13/03/17.
 */
public class CreateGameObjects {

    private List<Tunnel> obstacleTunnel = new ArrayList<>();
    private List<Block> obstacleBlock = new ArrayList<>();
    private List<Piece> obstaclePiece = new ArrayList<>();

    private TaskManager tmanager;

    public CreateGameObjects(TaskManager tmanager){
        this.tmanager = tmanager;

        this.createObstacleTunnel();
        this.createObstacleBlock();
        this.createObstaclePiece();
    }

    private void createObstacleTunnel(){
        obstacleTunnel.add(new Tunnel(600, 230, this.tmanager));
        obstacleTunnel.add(new Tunnel(1000, 230, this.tmanager));
        obstacleTunnel.add(new Tunnel(1600, 230, this.tmanager));
        obstacleTunnel.add(new Tunnel(1900, 230, this.tmanager));
        obstacleTunnel.add(new Tunnel(2500, 230, this.tmanager));
        obstacleTunnel.add(new Tunnel(3000, 230, this.tmanager));
        obstacleTunnel.add(new Tunnel(3800, 230, this.tmanager));
        obstacleTunnel.add(new Tunnel(4500, 230, this.tmanager));
    }

    private void createObstacleBlock(){
        obstacleBlock.add(new Block(400, 180, this.tmanager));
        obstacleBlock.add(new Block(1200, 180, this.tmanager));
        obstacleBlock.add(new Block(1270, 170, this.tmanager));
        obstacleBlock.add(new Block(1340, 160, this.tmanager));
        obstacleBlock.add(new Block(2000, 180, this.tmanager));
        obstacleBlock.add(new Block(2600, 160, this.tmanager));
        obstacleBlock.add(new Block(2650, 180, this.tmanager));
        obstacleBlock.add(new Block(3500, 160, this.tmanager));
        obstacleBlock.add(new Block(3550, 140, this.tmanager));
        obstacleBlock.add(new Block(4000, 170, this.tmanager));
        obstacleBlock.add(new Block(4200, 200, this.tmanager));
        obstacleBlock.add(new Block(4300, 210, this.tmanager));
    }

    private void createObstaclePiece(){
        obstaclePiece.add(new Piece(402, 145, this.tmanager));
        obstaclePiece.add(new Piece(1202, 140, this.tmanager));
        obstaclePiece.add(new Piece(1272, 95, this.tmanager));
        obstaclePiece.add(new Piece(1342, 40, this.tmanager));
        obstaclePiece.add(new Piece(1650, 145, this.tmanager));
        obstaclePiece.add(new Piece(2650, 145, this.tmanager));
        obstaclePiece.add(new Piece(3000, 135, this.tmanager));
        obstaclePiece.add(new Piece(3400, 125, this.tmanager));
        obstaclePiece.add(new Piece(4200, 145, this.tmanager));
        obstaclePiece.add(new Piece(4600, 40, this.tmanager));
    }

    public List<Tunnel> getOstacleTunnel(){ return Collections.unmodifiableList(this.obstacleTunnel); }

    public List<Block> getObstacleBlock(){
        return Collections.unmodifiableList(this.obstacleBlock);
    }

    public List<Piece> getObstaclePiece(){
        return Collections.unmodifiableList(this.obstaclePiece);
    }

}
