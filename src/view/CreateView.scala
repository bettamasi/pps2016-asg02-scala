package view

import game.Audio
import javafx.concurrent.Task
import model.TaskManager
import utils.Res
import utils.Utils
import javax.swing._
import java.awt._

/**
  * Created by BettaMasi on 13/03/17.
  */
object CreateView {
  private val X_POSITION_CASTLE = 4850
  private val Y_POSITION_CASTLE = 145
  private val FREQUENCY_OF_MARIO = 25
  private val FREQUENCY_OF_MUSHROOM = 45
  private val FREQUENCY_TURTLE = 45
  private val MUSHROOM_DEAD_OFFSET_Y = 20
  private val TURTLE_DEAD_OFFSET_Y = 30
}

class CreateView(var tmanager: TaskManager) extends JPanel {

  private val imgBackground1 : Image = Utils.getImage(Res.IMG_BACKGROUND)
  private val imgBackground2 : Image = Utils.getImage(Res.IMG_BACKGROUND)
  private val firstCastle : Image = Utils.getImage(Res.IMG_CASTLE)
  private val imgStart :Image = Utils.getImage(Res.START_ICON)
  private val finishCastle : Image = Utils.getImage(Res.IMG_CASTLE_FINAL)

  private var xpositionBackgroundUp = -50
  private var xpositionBackgroundUnder = -50

  override def paintComponent(g: Graphics): Unit = {
    super.paintComponent(g)
    this.updateBackgroundOnMovement()
    g.drawImage(this.imgBackground1, this.xpositionBackgroundUp, 0, null)
    g.drawImage(this.imgBackground2, this.xpositionBackgroundUnder, 0, null)
    g.drawImage(this.firstCastle, 10 - this.tmanager.getxPositionScreen, 95, null)
    g.drawImage(this.imgStart, 220 - this.tmanager.getxPositionScreen, 234, null)

    for(i <- 0 until this.tmanager.getObstacle.size){
      g.drawImage(this.tmanager.getObstacle.get(i).getImgObj, this.tmanager.getObstacle.get(i).getxPositionObject, this.tmanager.getObstacle.get(i).getyPositionObject, null)
    }

    for(i <- 0 until this.tmanager.getPieces.size){
      g.drawImage(this.tmanager.getPieces.get(i).imageOnMovement, this.tmanager.getPieces.get(i).getxPositionObject, this.tmanager.getPieces.get(i).getyPositionObject, null)
    }

    g.drawImage(this.tmanager.getFinishFlag.getImgObj, this.tmanager.getFinishFlag.getxPositionObject(), this.tmanager.getFinishFlag.getyPositionObject(), null)
    g.drawImage(this.finishCastle, CreateView.X_POSITION_CASTLE - this.tmanager.getxPositionScreen, CreateView.Y_POSITION_CASTLE, null)
    if (this.tmanager.getMario.isJumping) g.drawImage(this.tmanager.getMario.doJump, this.tmanager.getMario.getxPositionObject, this.tmanager.getMario.getyPositionObject, null)
    else g.drawImage(this.tmanager.getMario.walk(Res.IMGP_CHARACTER_MARIO, CreateView.FREQUENCY_OF_MARIO), this.tmanager.getMario.getxPositionObject, this.tmanager.getMario.getyPositionObject, null)
    if (this.tmanager.getMushroom.getCaracterIsAlive) g.drawImage(this.tmanager.getMushroom.walk(Res.IMGP_CHARACTER_MUSHROOM, CreateView.FREQUENCY_OF_MUSHROOM), this.tmanager.getMushroom.getxPositionObject, this.tmanager.getMushroom.getyPositionObject, null)
    else g.drawImage(this.tmanager.getMushroom.deadImage, this.tmanager.getMushroom.getxPositionObject, this.tmanager.getMushroom.getyPositionObject + CreateView.MUSHROOM_DEAD_OFFSET_Y, null)
    if (this.tmanager.getTurtle.getCaracterIsAlive) g.drawImage(this.tmanager.getTurtle.walk(Res.IMGP_CHARACTER_TURTLE, CreateView.FREQUENCY_TURTLE), this.tmanager.getTurtle.getxPositionObject, this.tmanager.getTurtle.getyPositionObject, null)
    else g.drawImage(this.tmanager.getTurtle.deadImage, this.tmanager.getTurtle.getxPositionObject, this.tmanager.getTurtle.getyPositionObject + CreateView.TURTLE_DEAD_OFFSET_Y, null)
  }

  private def updateBackgroundOnMovement() = {
    if (this.tmanager.getxPositionScreen >= 0 && this.tmanager.getxPositionScreen <= tmanager.getLenghMap) {
      this.tmanager.setxPositionScreen()
      // Moving the screen to give the impression that Mario is walking
      this.xpositionBackgroundUp = this.xpositionBackgroundUp - this.tmanager.getMarioMovement
      this.xpositionBackgroundUnder = this.xpositionBackgroundUnder - this.tmanager.getMarioMovement
    }
    this.checkBackgroundPosition()
  }

  private def checkBackgroundPosition() = { // Flipping between background1 and background2
    if (this.xpositionBackgroundUp == -800) this.xpositionBackgroundUp = 800
    else if (this.xpositionBackgroundUnder == -800) this.xpositionBackgroundUnder = 800
    else if (this.xpositionBackgroundUp == 800) this.xpositionBackgroundUp = -800
    else if (this.xpositionBackgroundUnder == 800) this.xpositionBackgroundUnder = -800
  }

  def playSoundPieces(): Unit = { Audio.playSound(Res.AUDIO_MONEY)}

  def setXpositionBackgroundUp(newXPositionBackgroundUp: Int): Unit = {this.xpositionBackgroundUp = newXPositionBackgroundUp}

  def setXpositionBackgroundUnder(newXPositionBackgroundUnder: Int): Unit = {this.xpositionBackgroundUnder = newXPositionBackgroundUnder}
}