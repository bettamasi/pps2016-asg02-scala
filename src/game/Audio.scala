package game

import javax.sound.sampled.AudioSystem
import javax.sound.sampled.Clip

object Audio {
  def playSound(song: String): Unit = {
    val s = new Audio(song)
    s.play()
  }
}

class Audio(val song: String) {
  private var clip : Clip = null

  try {
    val audio = AudioSystem.getAudioInputStream(getClass.getResource(song))
    clip = AudioSystem.getClip
    clip.open(audio)
  } catch {
    case e: Exception => println(e)
  }


  def play(): Unit = clip.start()
}