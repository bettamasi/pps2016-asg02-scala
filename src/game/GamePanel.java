package game;

import model.TaskManager;

import javax.swing.*;

/**
 * Created by BettaMasi on 15/03/17.
 */
public class GamePanel {

    private static final int WINDOW_WIDTH = 700;
    private static final int WINDOW_HEIGHT = 360;
    private static final String WINDOW_TITLE = "Super Mario";

    public GamePanel(){
        JFrame window = new JFrame(WINDOW_TITLE);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.pack();
        window.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        window.setLocationRelativeTo(null);
        window.setResizable(true);
        window.setVisible(true);
        Audio.playSound("/resources/audio/start.wav");

        Thread timer = new Thread(new TaskManager(window));
        timer.start();
    }
}
